<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Admin\Form;

use Laminas\Form\Form;

class PrintTemplateForm extends Form
{
    public function __construct($name = 'region-form', array $options = [])
    {
        parent::__construct($name, $options);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'hidden',
            'name'  => 'templateBody',
            'attributes'    => [
                'id'            => 'templateBody',
            ]
        ]);

        $this->add([
            'type'  => 'checkbox',
            'name'  => 'templateState',
            'attributes' => [
                'value' => 1
            ]
        ]);
    }

    protected function addInputFilter()
    {
        $inputFilter = $this->getInputFilter();

        $inputFilter->add([
            'name'      => 'templateBody',
            'required'  => false
        ]);

        $inputFilter->add([
            'name'      => 'adminState',
            'required'  => false,
            'validators'=> [
                [
                    'name'      => 'InArray',
                    'options'   => [
                        'haystack'  => [0, 1]
                    ]
                ]
            ]
        ]);
    }
}