<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * 打印模板
 * @ORM\Entity(repositoryClass="Admin\Repository\PrintTemplateRepository")
 * @ORM\Table(name="dberp_print_template")
 */
class PrintTemplate extends BaseEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="template_id", type="integer", length=11)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $templateId;

    /**
     * 打印模板标题
     * @ORM\Column(name="template_title", type="string", length=100)
     */
    private $templateTitle;

    /**
     * 打印模板内容
     * @ORM\Column(name="template_body", type="text")
     */
    private $templateBody;

    /**
     * 打印模板标记
     * @ORM\Column(name="template_code", type="string", length=50)
     */
    private $templateCode;

    /**
     * 打印模板状态，1 启用，0 禁用
     * @ORM\Column(name="template_state", type="integer", length=1)
     */
    private $templateState;

    /**
     * @return mixed
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    /**
     * @param mixed $templateId
     */
    public function setTemplateId($templateId): void
    {
        $this->templateId = $templateId;
    }

    /**
     * @return mixed
     */
    public function getTemplateTitle()
    {
        return $this->templateTitle;
    }

    /**
     * @param mixed $templateTitle
     */
    public function setTemplateTitle($templateTitle): void
    {
        $this->templateTitle = $templateTitle;
    }

    /**
     * @return mixed
     */
    public function getTemplateBody()
    {
        return $this->templateBody;
    }

    /**
     * @param mixed $templateBody
     */
    public function setTemplateBody($templateBody): void
    {
        $this->templateBody = $templateBody;
    }

    /**
     * @return mixed
     */
    public function getTemplateCode()
    {
        return $this->templateCode;
    }

    /**
     * @param mixed $templateCode
     */
    public function setTemplateCode($templateCode): void
    {
        $this->templateCode = $templateCode;
    }

    /**
     * @return mixed
     */
    public function getTemplateState()
    {
        return $this->templateState;
    }

    /**
     * @param mixed $templateState
     */
    public function setTemplateState($templateState): void
    {
        $this->templateState = $templateState;
    }
}