<?php
return [
    'host' => 'localhost',
    'user' => 'root',
    'password' => 'root',
    'dbname' => 'dberp',
    'port' => '3306',
    'charset' => 'utf8mb4'
];